package inventory.errors;

public class InvalidMinInhousePart extends RuntimeException {
    public InvalidMinInhousePart() {
    }

    public InvalidMinInhousePart(String message) {
        super(message);
    }
}
