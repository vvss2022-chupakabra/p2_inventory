package inventory.errors;

public class InvalidPriceInhousePart extends RuntimeException {

    public InvalidPriceInhousePart() {
    }

    public InvalidPriceInhousePart(String message) {
        super(message);
    }
}
