package inventory.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;

class InhousePartTestWithMock {

    @Mock
    private InhousePart inhousePart;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getMachineId() {
        Mockito.when(this.inhousePart.getMachineId()).thenReturn(1);
        assertEquals(1,this.inhousePart.getMachineId());
    }

    @Test
    void testToString() {
        Mockito.when(this.inhousePart.toString()).thenReturn("gigi");
        assertEquals("gigi",this.inhousePart.toString());
    }
}