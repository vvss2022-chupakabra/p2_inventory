package inventory.repository;

import inventory.model.OutsourcedPart;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;

class InMemoryRepositoryTestWithMock {

    @Mock
    private InMemoryRepository inMemoryRepository;

    @Mock
    private OutsourcedPart outsourcedPart;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getAllParts() {
        Mockito.when(this.inMemoryRepository.getAllParts()).thenReturn(FXCollections.observableArrayList(outsourcedPart));
        assertEquals(1,this.inMemoryRepository.getAllParts().size());
    }

    @Test
    void lookupPart() {
        Mockito.when(this.inMemoryRepository.lookupPart("a")).thenReturn(outsourcedPart);
        assertEquals(outsourcedPart,this.inMemoryRepository.lookupPart("a"));
    }
}