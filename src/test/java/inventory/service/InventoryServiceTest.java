package inventory.service;
import inventory.errors.InvalidMinInhousePart;
import inventory.errors.InvalidPriceInhousePart;
import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.FileRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.io.PrintWriter;

import static org.junit.jupiter.api.Assertions.*;

class InventoryServiceTest {
    private FileRepository fileRepository;
    private InventoryService inventoryService;
    private String testFilePath;

    @Test
    @DisplayName("addInhousePart")
    @Tag("addInhousePart")
    void addInhousePartECPSucces(){
        this.inventoryService.addInhousePart("a",2.0,10,2,12,2);
        assertEquals(1,this.inventoryService.getAllParts().size());
    }

    @Test
    void addInhousePartECPFailPrice(){
        this.inventoryService.addInhousePart("a",2.0,10,2,12,2);
        Assertions.assertThrows(InvalidPriceInhousePart.class,()->{
            this.inventoryService.addInhousePart("a", -1, 10, 2, 12, 2);
        });
    }

    @Test
    void addInhousePartECPFailMin(){
        this.inventoryService.addInhousePart("a",2.0,10,2,12,2);
        Assertions.assertThrows(InvalidMinInhousePart.class,()->{
            this.inventoryService.addInhousePart("a", 10, 10, -2, 12, 2);
        });
    }

    @Test
    void addInhousePartECPFailPriceMin(){
        this.inventoryService.addInhousePart("a",2.0,10,2,12,2);
        Assertions.assertThrows(InvalidPriceInhousePart.class,()->{
            this.inventoryService.addInhousePart("a", -1, 10, -2, 12, 2);
        });
    }

    @Test
    void addInhousePartBVASucces(){
        this.inventoryService.addInhousePart("a",2.0,10,2,12,2);
        assertEquals(1,this.inventoryService.getAllParts().size());
    }

    @Test
    void addInhousePartBVAFailPriceLow(){
        this.inventoryService.addInhousePart("a",2.0,10,2,12,2);
        Assertions.assertThrows(InvalidPriceInhousePart.class,()->{
            this.inventoryService.addInhousePart("a", -1, 10, 2, 12, 2);
        });
    }

    @Test
    void addInhousePartBVAFailPriceBoundary(){
        this.inventoryService.addInhousePart("a",2.0,10,2,12,2);
        Assertions.assertThrows(InvalidPriceInhousePart.class,()->{
            this.inventoryService.addInhousePart("a", 0, 10, 2, 12, 2);
        });
    }

    @Test
    void addInhousePartBVAFailMinLow(){
        this.inventoryService.addInhousePart("a",2.0,10,2,12,2);
        Assertions.assertThrows(InvalidMinInhousePart.class,()->{
            this.inventoryService.addInhousePart("a", 10, 10, -2, 12, 2);
        });
    }

    @Test
    void addInhousePartBVAFailMinBoundary(){
        this.inventoryService.addInhousePart("a",2.0,10,2,12,2);
        Assertions.assertThrows(InvalidMinInhousePart.class,()->{
            this.inventoryService.addInhousePart("a", 10, 10, 0, 12, 2);
        });
    }

    @Test
    void addInhousePartBVAFailPriceLowMinLow(){
        this.inventoryService.addInhousePart("a",2.0,10,2,12,2);
        Assertions.assertThrows(InvalidPriceInhousePart.class,()->{
            this.inventoryService.addInhousePart("a", -1, 10, -2, 12, 2);

        });
    }

    private void addOutsourcePartECP(){
        this.inventoryService.addOutsourcePart("a",2.0,10,2,12,"2");
        assertEquals(1,this.inventoryService.getAllParts().size());
        try {
            this.inventoryService.addOutsourcePart("a", -1, 10, 2, 12, "2");
            assert false;
        }
        catch (RuntimeException e){
            assertEquals(1,this.inventoryService.getAllParts().size());
            assertEquals("Price is invalid!",e.getMessage());
        }
        try {
            this.inventoryService.addOutsourcePart("a", 10, 10, -2, 12, "2");
            assert false;
        }
        catch (RuntimeException e){
            assertEquals(1,this.inventoryService.getAllParts().size());
            assertEquals("Min is invalid!",e.getMessage());
        }
        try {
            this.inventoryService.addOutsourcePart("a", -1, 10, -2, 12, "2");
            assert false;
        }
        catch (RuntimeException e){
            assertEquals(1,this.inventoryService.getAllParts().size());
            assertEquals("Price is invalid!",e.getMessage());
        }
    }

    private void addOutsourcePartBVA(){
        this.inventoryService.addOutsourcePart("a",2.0,10,2,12,"2");
        assertEquals(2,this.inventoryService.getAllParts().size());
        try {
            this.inventoryService.addOutsourcePart("a", -1, 10, 2, 12, "2");
            assert false;
        }
        catch (RuntimeException e){
            assertEquals(2,this.inventoryService.getAllParts().size());
            assertEquals("Price is invalid!",e.getMessage());
        }
        try {
            this.inventoryService.addOutsourcePart("a", 0, 10, 2, 12, "2");
            assert false;
        }
        catch (RuntimeException e){
            assertEquals(2,this.inventoryService.getAllParts().size());
            assertEquals("Price is invalid!",e.getMessage());
        }
        try {
            this.inventoryService.addOutsourcePart("a", 10, 10, -2, 12, "2");
            assert false;
        }
        catch (RuntimeException e){
            assertEquals(2,this.inventoryService.getAllParts().size());
            assertEquals("Min is invalid!",e.getMessage());
        }
        try {
            this.inventoryService.addOutsourcePart("a", 10, 10, 0, 12, "2");
            assert false;
        }
        catch (RuntimeException e){
            assertEquals(2,this.inventoryService.getAllParts().size());
            assertEquals("Min is invalid!",e.getMessage());
        }
        try {
            this.inventoryService.addOutsourcePart("a", -1, 10, -2, 12, "2");
            assert false;
        }
        catch (RuntimeException e){
            assertEquals(2,this.inventoryService.getAllParts().size());
            assertEquals("Price is invalid!",e.getMessage());
        }
    }

    private void addProductECP(){
        Part inhousePart1 = new OutsourcedPart(1,"a",2.0,10,2,12,"1");
        Part inhousePart2 = new OutsourcedPart(2,"b",2.0,10,2,12,"2");
        Part outSourcePart1 = new OutsourcedPart(3,"a",2.0,10,2,12,"3");
        Part outSourcePart2 = new OutsourcedPart(4,"b",2.0,10,2,12,"4");
        ObservableList<Part> parts = FXCollections.observableArrayList(inhousePart1,inhousePart2,outSourcePart1,outSourcePart2);
        this.inventoryService.addProduct("a",2.0,10,2,12,parts);
        assertEquals(1,this.inventoryService.getAllProducts().size());
        try{
            this.inventoryService.addProduct("a",-2,10,2,12,parts);
            assert false;
        }
        catch (RuntimeException e){
            assertEquals(1,this.inventoryService.getAllProducts().size());
            assertEquals("Price is invalid!",e.getMessage());
        }
        try{
            this.inventoryService.addProduct("a",2,10,-2,12,parts);
            assert false;
        }
        catch (RuntimeException e){
            assertEquals(1,this.inventoryService.getAllProducts().size());
            assertEquals("Min is invalid!",e.getMessage());
        }
        try{
            this.inventoryService.addProduct("a",-2,10,-2,12,parts);
            assert false;
        }
        catch (RuntimeException e){
            assertEquals(1,this.inventoryService.getAllProducts().size());
            assertEquals("Price is invalid!",e.getMessage());
        }
    }

    private void addProductBVA(){
        Part inhousePart1 = new OutsourcedPart(1,"a",2.0,10,2,12,"1");
        Part inhousePart2 = new OutsourcedPart(2,"b",2.0,10,2,12,"2");
        Part outSourcePart1 = new OutsourcedPart(3,"a",2.0,10,2,12,"3");
        Part outSourcePart2 = new OutsourcedPart(4,"b",2.0,10,2,12,"4");
        ObservableList<Part> parts = FXCollections.observableArrayList(inhousePart1,inhousePart2,outSourcePart1,outSourcePart2);
        this.inventoryService.addProduct("a",2.0,10,2,12,parts);
        assertEquals(2,this.inventoryService.getAllProducts().size());
        try{
            this.inventoryService.addProduct("a",-2,10,2,12,parts);
            assert false;
        }
        catch (RuntimeException e){
            assertEquals(2,this.inventoryService.getAllProducts().size());
            assertEquals("Price is invalid!",e.getMessage());
        }
        try{
            this.inventoryService.addProduct("a",0,10,2,12,parts);
            assert false;
        }
        catch (RuntimeException e){
            assertEquals(2,this.inventoryService.getAllProducts().size());
            assertEquals("Price is invalid!",e.getMessage());
        }
        try{
            this.inventoryService.addProduct("a",2,10,-2,12,parts);
            assert false;
        }
        catch (RuntimeException e){
            assertEquals(2,this.inventoryService.getAllProducts().size());
            assertEquals("Min is invalid!",e.getMessage());
        }
        try{
            this.inventoryService.addProduct("a",2,10,0,12,parts);
            assert false;
        }
        catch (RuntimeException e){
            assertEquals(2,this.inventoryService.getAllProducts().size());
            assertEquals("Min is invalid!",e.getMessage());
        }
        try{
            this.inventoryService.addProduct("a",-2,10,-2,12,parts);
            assert false;
        }
        catch (RuntimeException e){
            assertEquals(2,this.inventoryService.getAllProducts().size());
            assertEquals("Price is invalid!",e.getMessage());
        }
    }

    @BeforeEach
    @Timeout(5)
    void setUp() {
        this.testFilePath = "data/items-test.txt";
        this.fileRepository = new FileRepository(this.testFilePath);
        this.inventoryService = new InventoryService(this.fileRepository);
    }

    @AfterEach
    @Timeout(5)
    void tearDown() throws IOException {
        ClassLoader classLoader = InventoryServiceTest.class.getClassLoader();
        PrintWriter writer = new PrintWriter(classLoader.getResource(this.testFilePath).getFile().replaceAll("%20"," "));
        writer.print("");
        writer.close();
    }

    @Test
    void whiteBox1(){
        Part inhousePart1 = new OutsourcedPart(1,"a",2.0,10,2,12,"1");
        Part inhousePart2 = new OutsourcedPart(2,"b",2.0,10,2,12,"2");
        Part outSourcePart1 = new OutsourcedPart(3,"a",2.0,10,2,12,"3");
        Part outSourcePart2 = new OutsourcedPart(4,"b",2.0,10,2,12,"4");
        ObservableList<Part> parts = FXCollections.observableArrayList(inhousePart1,inhousePart2,outSourcePart1,outSourcePart2);
        this.inventoryService.addProduct("masina",2.0,10,2,12,parts);
        assertNull(this.inventoryService.lookupProduct("3"));
    }

    @Test
    void whiteBox2(){
        Part inhousePart1 = new OutsourcedPart(1,"a",2.0,10,2,12,"1");
        Part inhousePart2 = new OutsourcedPart(2,"b",2.0,10,2,12,"2");
        Part outSourcePart1 = new OutsourcedPart(3,"a",2.0,10,2,12,"3");
        Part outSourcePart2 = new OutsourcedPart(4,"b",2.0,10,2,12,"4");
        ObservableList<Part> parts = FXCollections.observableArrayList(inhousePart1,inhousePart2,outSourcePart1,outSourcePart2);
        Product product=new Product(1,"masina",2.0,10,2,12,parts);
        this.inventoryService.addProduct(product.getName(),product.getPrice(),product.getInStock(),product.getMin(),product.getMax(),parts);
        assertEquals(product,this.inventoryService.lookupProduct("1"));
    }

    @Test
    void whiteBox3(){
        Part inhousePart1 = new OutsourcedPart(1,"a",2.0,10,2,12,"1");
        Part inhousePart2 = new OutsourcedPart(2,"b",2.0,10,2,12,"2");
        Part outSourcePart1 = new OutsourcedPart(3,"a",2.0,10,2,12,"3");
        Part outSourcePart2 = new OutsourcedPart(4,"b",2.0,10,2,12,"4");
        ObservableList<Part> parts = FXCollections.observableArrayList(inhousePart1,inhousePart2,outSourcePart1,outSourcePart2);
        Product product=new Product(1,"masina",2.0,10,2,12,parts);
        this.inventoryService.addProduct(product.getName(),product.getPrice(),product.getInStock(),product.getMin(),product.getMax(),parts);
        assertEquals(product,this.inventoryService.lookupProduct("mas"));
    }

    @Test
    void whiteBox4(){
        assertEquals(Product.class,this.inventoryService.lookupProduct("mas").getClass());
    }




    @Test
    @RepeatedTest(4)
    void addOutsourcePart() {
        this.addOutsourcePartECP();
        this.addOutsourcePartBVA();
    }

    @Test
    @Disabled("allready enough tests :)")
    void addProduct() {
        this.addProductECP();
        this.addProductBVA();
    }
}