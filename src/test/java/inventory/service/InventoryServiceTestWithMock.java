package inventory.service;

import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.repository.FileRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;

class InventoryServiceTestWithMock {

    @Mock
    private OutsourcedPart outsourcedPart;

    @Mock
    private FileRepository fileRepository;

    @InjectMocks
    private InventoryService inventoryService;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getAllParts() {
        Mockito.when(this.inventoryService.getAllParts()).thenReturn(FXCollections.observableArrayList(outsourcedPart));
        ObservableList<Part> l=this.inventoryService.getAllParts();
        assertEquals(outsourcedPart,l.get(0));
    }

    @Test
    void lookupPart() {
        Mockito.when(this.inventoryService.lookupPart(outsourcedPart.getName())).thenReturn(null);
        assertNull(this.inventoryService.lookupPart(outsourcedPart.getName()));
    }

    @Test
    void getAllPartsIntegrationR(){
        Mockito.when(this.fileRepository.getAllParts()).thenReturn(FXCollections.observableArrayList());
        ObservableList<Part> l=this.inventoryService.getAllParts();
        assertEquals(0,l.size());
    }

    @Test
    void lookupPartIntegrationR() {
        Mockito.when(this.fileRepository.lookupPart("a")).thenReturn(outsourcedPart);
        assertEquals(outsourcedPart, inventoryService.lookupPart("a"));
    }

    @Test
    void getAllPartsIntegrationE(){
        Mockito.when(this.fileRepository.getAllParts()).thenReturn(FXCollections.observableArrayList(outsourcedPart));
        Mockito.when(outsourcedPart.getPrice()).thenReturn(2.0);
        ObservableList<Part> l=this.inventoryService.getAllParts();
        assertEquals(2.0,l.get(0).getPrice());
    }

    @Test
    void lookupPartIntegrationE() {
        Mockito.when(this.fileRepository.lookupPart("a")).thenReturn(outsourcedPart);
        Mockito.when(outsourcedPart.getName()).thenReturn("b");
        assertEquals("b", inventoryService.lookupPart("a").getName());
    }
}